def reverser
  yield.split.reduce([]) {|acc, word| acc << word.reverse}.join(" ")
end

def adder(num_to_add=1)
  yield + num_to_add
end

def repeater(num_repeats=1, &proc)
  num_repeats.times {proc.call}
end
