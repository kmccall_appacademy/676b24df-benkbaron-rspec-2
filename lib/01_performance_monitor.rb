def measure(n=1, &stub)
  start = Time.now
  n.times {stub.call}
  finish = Time.now

  (finish - start) / n
end
